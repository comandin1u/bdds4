<?php
require 'vendor/autoload.php';
use \bdd\controler\Controler;
use \bdd\controler\Controler2;
use \bdd\controler\Controler3;
use \Illuminate\Database\Capsule\Manager as db;

try {
	$tab = parse_ini_file('src/conf/config.ini');
	$db = new \Illuminate\Database\Capsule\Manager();
	$db->addConnection($tab);
	$db->setAsGlobal();
	$db->bootEloquent();
	$db::connection()->enableQueryLog();
} catch (PDOException $e) {
	echo $e;
}

$app=new Slim\Slim();



// Seance 1 
$app->get('/', function(){
	(new Controler())->debut();	
})->name('racine');

$app->get('/r2', function(){
	(new Controler())->lister_Japon();	
})->name('liste_japon');

$app->get('/r3', function(){
	(new Controler())->lister_plateforme();	
})->name('liste_plateforme');

$app->get('/r4', function(){
	(new Controler())->lister_Jeu();	
})->name('liste_Jeu');

$app->get('/r5', function(){
	(new Controler())->pagination();	
})->name('Pagination');


//Seance 2 

$app->get('/s2r1', function(){
	(new Controler2())->r1();
})->name('s2r1');

$app->get('/s2r2', function(){
	(new Controler2())->r2();
})->name('s2r2');

$app->get('/s2r3', function(){
	(new Controler2())->r3();
})->name('s2r3');

$app->get('/s2r4', function(){
	(new Controler2())->r4();
})->name('s2r4');

$app->get('/s2r5', function(){
	(new Controler2())->r5();
})->name('s2r5');

$app->get('/s2r6', function(){
	(new Controler2())->r6();
})->name('s2r6');

$app->get('/s2r7', function(){
	(new Controler2())->r7();
})->name('s2r7');

//Seance 3

$app->get('/s3r1', function(){
	(new Controler3())->r1();
})->name('s3r1');

$app->get('/s3r2', function(){
	(new Controler3())->r2();
})->name('s3r2');

$app->get('/s3r3', function(){
	(new Controler3())->r3();
})->name('s3r3');

$app->get('/s3r4', function(){
	(new Controler3())->r4();
})->name('s3r4');

$app->get('/s3r5', function(){

echo " Avant la création de l'index : " . "<br>" .
 " 2. lister les jeux dont le nom contient Mario " . "<br>" .  "<br>" .
"Execution du script 2.9338529109955 secondes" . "<br>" .
"test avec Sonic " . "<br>" . "<br>" .
"Execution du script 2.5240669250488 secondes" . "<br>" .
"test avec Zelda" . "<br>" . "<br>" .
"Execution du script 1.1046078205109 secondes" . "<br>" . "<br>" . "<br>" . "<br>" .
" Après la création de l'index : " . "<br>" . "<br>"  ;

	(new Controler3())->r2();
	echo "<br>";
	(new Controler3())->r2_2();
	echo "<br>";
	(new Controler3())->r2_3();
})->name('s3r5');

$app->get('/s3r6', function(){
	(new Controler3())->r5();
})->name('s3r6');


$app->get('/s3r7', function(){
	(new Controler3())->r6();
	$queries = db::getQueryLog();
	var_dump($queries);
})->name('s3r7');


$app->get('/s3r8', function(){
	(new Controler3())->r7();
	$queries = db::getQueryLog();
	var_dump($queries);
})->name('s3r8');

$app->get('/s3r9', function(){
	(new Controler3())->r8();
	$queries = db::getQueryLog();
	var_dump($queries);
})->name('s3r9');


$app->get('/s3r10', function(){
	(new Controler3())->r9();
	$queries = db::getQueryLog();
	var_dump($queries);
})->name('s3r10');

$app->get('/s3r11', function(){
	(new Controler3())->r10();
	$queries = db::getQueryLog();
	var_dump($queries);
})->name('s3r11');


$app->run();
?>