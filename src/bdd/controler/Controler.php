<?php
namespace bdd\controler;
use \bdd\vue\vue;
use \bdd\model\Game;
use \bdd\model\Company;
use \bdd\model\Plateform;
class Controler{
	
	public function debut(){
		$r=Game::select("name")->where("name","like","%Mario%")->get();
		$d=new vue($r);
		$d->render(vue::DEBUT);
	}
	
	
	public function lister_Japon(){
		$r=Company::select("name")->where("location_country","=","Japan")->get();
		$d=new vue($r);
		$d->render(vue::DEBUT);
	}

	public function lister_plateforme(){
		$r=Plateform::select("name")->where("install_base",">=",10000000)->get();
		$d=new vue($r);
		$d->render(vue::DEBUT);
	}
	
	public function lister_Jeu(){
		$r=Game::select("id","name")->take(442)->skip(21172)->get();
		$d=new vue($r);
		$d->render(vue::DEBUT);
	}
	
	public function pagination(){
		$r=Game::select("name","deck")->simplePaginate(500);
		$d=new vue($r);
		$d->render(vue::DEBUT);
	}
	
	
	
	
}
	