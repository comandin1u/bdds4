<?php
namespace bdd\controler;
use \bdd\vue\vue2;
use bdd\model\Character;
use bdd\model\Company;
use bdd\model\Game;
use bdd\model\Rating_board;
use bdd\model\GameDevelopers;
use bdd\model\Game2character;
use bdd\model\Plateform;
use bdd\model\GameRating;
use bdd\model\Theme;

class Controler3{

	
	public function r1(){
		$timestamp_debut = microtime(true);
		$r=Game::find(12342);
		$listCh = $r->model1()->get();
		$timestamp_fin = microtime(true);
		$difference_ms = $timestamp_fin - $timestamp_debut;
		
		// foreach($listCh as $v){
			// echo $v->name."<br>";
		// }
		echo "<br>" . "1. lister l'ensemble des jeux " . "<br>";
		echo 'Execution du script ' . $difference_ms . ' secondes';
	}
	
	
	
	
		public function r2(){
		$timestamp_debut = microtime(true);
			$a=Game::select("id")->where("name","like","Mario%")->get();
			foreach($a as $v=>$k){
				$r=Game::find($k['id']);
				$l=$r->model1()->get();
				$timestamp_fin = microtime(true);
				$difference_ms = $timestamp_fin - $timestamp_debut;
				// foreach($l as $v){
					// $v1=$v->name."<br>";
					// $d=new vue2($v1);
					// $d->render(vue2::R1);
				// }
			}
			echo "<br>" . "2. lister les jeux dont le nom contient Mario" . "<br>";
			echo 'Execution du script ' . $difference_ms . ' secondes';
			
	}
	
	
	/* 
	*requete avec une deuxieme valeur
	*/
		public function r2_2(){
		$timestamp_debut = microtime(true);
			$a=Game::select("id")->where("name","like","Sonic%")->get();
			foreach($a as $v=>$k){
				$r=Game::find($k['id']);
				$l=$r->model1()->get();
				$timestamp_fin = microtime(true);
				$difference_ms = $timestamp_fin - $timestamp_debut;
				// foreach($l as $v){
					// $v1=$v->name."<br>";
					// $d=new vue2($v1);
					// $d->render(vue2::R1);
				// }
			}
			echo "<br>" . "test avec Sonic " . "<br>";
			echo 'Execution du script ' . $difference_ms . ' secondes';
			
	}
	
	/* 
	*requete avec une Troisieme valeur
	*/
		public function r2_3(){
		$timestamp_debut = microtime(true);
			$a=Game::select("id")->where("name","like","Zelda%")->get();
			foreach($a as $v=>$k){
				$r=Game::find($k['id']);
				$l=$r->model1()->get();
				$timestamp_fin = microtime(true);
				$difference_ms = $timestamp_fin - $timestamp_debut;
				// foreach($l as $v){
					// $v1=$v->name."<br>";
					// $d=new vue2($v1);
					// $d->render(vue2::R1);
				// }
			}
			echo "<br>" . "test avec Zelda " . "<br>";
			echo 'Execution du script ' . $difference_ms . ' secondes';
			
	}
	
	public function r3(){
			$timestamp_debut = microtime(true);
			$a=Company::where('name','like','%Sony%')->get();
			foreach($a as $g){
				$res = $g->game_developers()->get();
				// foreach($res as $jeu){
					// echo $jeu->name. "<br>";
				// }
			}
			$timestamp_fin = microtime(true);
			$difference_ms = $timestamp_fin - $timestamp_debut;
			echo "<br>" . "3. Afficher les personnages des jeux dont le débute par Mario " . "<br>";
			echo 'Execution du script ' . $difference_ms . ' secondes';
	}
	
	public function r4(){
		$timestamp_debut = microtime(true);
		$a = Game::where('name','like','%Mario%');
		foreach($a as $g){
			$res = $g->game_rating()->where('name','like','%3+%')->get();
			// foreach($res as $jeu){
				// echo $jeu->name . "<br>";
			// }
		
		}		
		$timestamp_fin = microtime(true);
		$difference_ms = $timestamp_fin - $timestamp_debut;
		echo "<br>" . "4. les jeux dont le nom débute par Mario et dont le rating initial contient 3+" . "<br>";
		echo 'Execution du script ' . $difference_ms . ' secondes';
	}
	
	
	//lister le nom des jeux dont le nom contient une valeur
	public function r5(){
		$timestamp_debut = microtime(true);
		$a=Game::select("name")->where("name","like","%Mario%")->get();
		foreach($a as $m){
			 echo $m->name . "<br>";
		 }
			$timestamp_fin = microtime(true);
			 $difference_ms = $timestamp_fin - $timestamp_debut;
				
			
			echo "<br>" . " Company dont le nom contient Mario " . "<br>";
			echo 'Execution du script ' . $difference_ms . ' secondes';
	
	}
	
	/* La requête contenant Mario est plus rapide que celle débutant par Mario car 
	* c'est une moins grande contrainte de vérifier que le nom du jeu contient Mario. 
	* C'est pour cette raison que cela est plus rapide. De plus, la présence d'un index 
	* augmente grandement le temps d'éxécution.
	*/
	
	
	
	//Lister les compagnies d'un pays
	
	

	
	
	
	
	
	/*Partie 2*/
	
		
		//liste des jeux contenant Mario
		//la fonction s'execute avec un log
		public function r6(){
			$timestamp_debut = microtime(true);
			$a=Game::select("name")->where("name","like","%Mario%")->get();
			foreach($a as $m){
				echo $m->name . "<br>";
			}
				$timestamp_fin = microtime(true);
				$difference_ms = $timestamp_fin - $timestamp_debut;
			echo "<br>" . " Company dont le nom contient Mario " . "<br>";
			echo 'Execution du script ' . $difference_ms . ' secondes';
		}
	
		//afficher le nom des personnages du jeu 12342
		//la fonction s'execute avec un log dans l'index
		public function r7(){
			$r=Game::find(12342);
			$listCh = $r->model1()->get();
			foreach($listCh as $v){
				echo $v->name."<br>";
			}
		}
	
		//afficher les noms des persons apparus pour la 1ere fois dans 1 jeu dont le nom contient Mario
		//la fonction s'execute avec un log dans l'index
		
		
		
		
		//Les noms des personnages des jeux dont le nom contient Mario
		//la fonction s'execute avec un log dans l'index
		public function r9(){
			$a=Game::select("id")->where("name","like","Mario%")->get();
			foreach($a as $v=>$k){
				$r=Game::find($k['id']);
				$l=$r->model1()->get();
				foreach($l as $v){
					$v1=$v->name."<br>";
					$d=new vue2($v1);
					$d->render(vue2::R1);
				}
			}	
			
	}
	
	
	//les jeux developpe par une compagnie dont le nom contient Sony
	//la fonction s'execute avec un log dans l'index
	public function r10(){
			$a=Company::where('name','like','%Sony%')->get();
			foreach($a as $g){
				$res = $g->game_developers()->get();
				foreach($res as $jeu){
					echo $jeu->name. "<br>";
				}
			}
	}
		
		
		
		
	
	
	
	
	
	
	
}
	
	
?>