<?php

namespace bdd\model;
	
class Company extends \Illuminate\Database\Eloquent\Model{
	protected $table='company';
	protected $primaryKey='id';
	public $timestamps=false;
	
		public function game_developers(){
			return $this->belongsToMany("\bdd\model\Game","game_developers","game_id","comp_id");
		}
		
		public function getCompany(){
			return Game::select("name")->get();
		}
			
		
}
?>