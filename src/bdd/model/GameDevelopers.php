<?php

namespace bdd\model;
	
class GameDevelopers extends \Illuminate\Database\Eloquent\Model{
	protected $table='game_developers';
	protected $primaryKey='game_id';
	public $timestamps=false;
}
?>