<?php

namespace bdd\model;

class Character extends \Illuminate\Database\Eloquent\Model{
	protected $table='character';
	protected $primaryKey='id';
	public $timestamps=false;
	
	
	public function model2(){
		return $this->belongsToMany("bdd\model\Game","game2character","game_id","character_id");
	}
	
	
	public function getGame(){
			return Game::select("name")->where("name","like","Mario%")->get();
		}
}
?>