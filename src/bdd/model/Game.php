<?php

namespace bdd\model;
	
class Game extends \Illuminate\Database\Eloquent\Model{
	protected $table='game';
	protected $primaryKey='id';
	public $timestamps=false;
	
		public function model1(){
			return $this->belongsToMany("bdd\model\Character","game2character","game_id","character_id");
		}
		
		// public function getGameRating(){
			// return GameRating::select("rating_board_id")->get();
		// }
		
		public function game_rating(){
			return $this->belongsToMany('bdd\model\GameRating','game2rating','game_id','rating_id');
		}
		
		public function game_publishers(){
			return $this->belongsToMany("\bdd\model\Company","game_publishers","game_id","comp_id");
		}
		
		
		public function getGameRt(){
			return GameRating::select("name")->where("name","like","%3+%")->get();
		}
}
?>